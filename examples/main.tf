variable "hcloud_token" {
  type = string
  sensitive = true
}

provider "hcloud" {
  token = var.hcloud_token
}

# Create a new SSH key
resource "hcloud_ssh_key" "default" {
  name       = "Terraform"
  public_key = file("~/.ssh/hetzner_key.pub")
}

# bereits existierenden ssh key hinterlegen
module "cluster" {
  source  = "git@gitlab.com:TabsVsSpaces/terraform-hcloud-k3s.git"
  # version      = "0.1.2"
  hcloud_token = var.hcloud_token
  ssh_keys     = [hcloud_ssh_key.default.id]

  master_type = "cx21"

  node_groups = {
    "cx11" = 1
    #"cx51" = 2
  }
}

output "master_ipv4" {
  depends_on  = [module.cluster]
  description = "Public IP Address of the master node"
  value       = module.cluster.master_ipv4
}

output "nodes_ipv4" {
  depends_on  = [module.cluster]
  description = "Public IP Address of the worker nodes"
  value       = module.cluster.nodes_ipv4
}
